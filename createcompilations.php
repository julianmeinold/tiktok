<?php
	function checkVideoId($videoIds, $videoId)
	{
		$videoIdFound=false;

		$numVideoIds=count($videoIds);

		for($i=0; $i<$numVideoIds; $i++)
		{
			if(strcmp($videoIds[$i], $videoId)==0)
			{
				$videoIdFound=true;

				break;
			}
		}

		return $videoIdFound;
	}

	function getVideoIdFromVideoFileName($videoFileName)
	{
		$videoFileNameNumBytes=strlen($videoFileName);

		$offset=strpos($videoFileName, "TikTok-")+7;

		$l=($videoFileNameNumBytes-$offset)-4;

		$videoId=substr($videoFileName, $offset, $l);

		return $videoId;
	}

	function getVideoIdsFromVideoArchive($videoArchivePath)
	{
		$videoIds=array();

		$videoFileNames=array();

		$videoFileNames=scandir($videoArchivePath);

		$numVideoFileNames=count($videoFileNames);

		for($i=0; $i<$numVideoFileNames; $i++)
		{
			if(strcmp($videoFileNames[$i], ".")!=0 && strcmp($videoFileNames[$i], "..")!=0 && strcmp($videoFileNames[$i], ".DS_Store")!=0)
				$videoIds[$numVideoIds++]=getVideoIdFromVideoFileName($videoFileNames[$i]);
		}

		return $videoIds;
	}

	function getNumCompilations($compilationsFileName)
	{
		$numCompilations=0;

		if(file_exists($compilationsFileName))
		{
			$buf=file_get_contents($compilationsFileName);

			$numCompilations=substr_count($buf, "\n");
		}

		return $numCompilations;
	}

	function getVideoIdsFromCompilations($compilationsFileName)
	{
		$videoIds=array();

		$numVideoIds=0;

		$buf=file_get_contents($compilationsFileName);

		$compilationsFileLines=explode("\n", $buf);

		$numCompilationsFileLines=count($compilationsFileLines);

		for($i=0; $i<$numCompilationsFileLines; $i++)
		{
			$compilation=array();

			$compilation=explode(" ", $compilationsFileLines[$i]);

			$numVideoIdsPerCompilation=count($compilation);

			for($j=0; $j<$numVideoIdsPerCompilation; $j++)
			{
				$videoIds[$numVideoIds++]=$compilation[$j];
			}
		}

		return $videoIds;
	}

	function checkCompilationsVideoId($videoId)
	{
		$videoIdFound=false;

		$compilationsFileName="compilations.txt";

		$compilationVideoIds=getVideoIdsFromCompilations($compilationsFileName);

		$numCompilationVideoIds=count($compilationVideoIds);

		for($i=0; $i<$numCompilationVideoIds; $i++)
		{
			if(strcmp($compilationVideoIds[$i], $videoId)==0)
			{
				$videoIdFound=true;

				break;
			}
		}

		return $videoIdFound;
	}

	function getRandomVideoIdFromArchive($videoArchivePath)
	{
		$videoId=0;

		$archiveVideoIds=array();

		$archiveVideoIds=getVideoIdsFromVideoArchive($videoArchivePath);

		$numArchiveVideoIds=count($archiveVideoIds);

		$videoId=$archiveVideoIds[rand(0, $numArchiveVideoIds-1)];

		return $videoId;
	}

	function getVideoFileNameFromVideoId($videoArchivePath, $videoId)
	{
		exec("ls ./".$videoArchivePath."/*".$videoId."*", $videoFilePath);

		$videoFileName=basename($videoFilePath[0]);

		return $videoFileName;
	}

	print("TikTok video compiler 1.0 by Julian Meinold\n\n");

	$compilationsFileName="compilations.txt";

	$videoArchivePath="videos";

	$numCompilations=$argv[1];

	$numVideosPerCompilation=$argv[2];

	$videos=array();

	for($i=0; $i<$numCompilations; $i++)
	{
		$numVideos=0;

		for($j=0; $j<$numVideosPerCompilation; $j++)
		{
			$videoId=getRandomVideoIdFromArchive($videoArchivePath);

			if(!checkCompilationsVideoId($videoId))
			{
				$videos[$numVideos++]=$videoId;

				$videoFileName=getVideoFileNameFromVideoId($videoArchivePath, $videoId);

				copy("./videos/".$videoFileName, "./tmp/".$videoId.".mp4");

				$command="ffmpeg -loop 1 -i ./data/background.png -i ./tmp/".$videoId.".mp4 -filter_complex \"[1]scale=-1:1080:flags=bicubic[scaled];[0][scaled]overlay=(main_w/2)-(overlay_w/2):0:shortest=1[scaledAndOverlayed]\" -map \"[scaledAndOverlayed]\" -map 1:1 -acodec copy ./tmp/".$videoId."_processed.mp4";

				system($command);

				unlink("./tmp/".$videoId.".mp4");

				rename("./videos/".$videoFileName, "./videos_processed/".$videoFileName);
			}
			else
			{
				$videoFileName=getVideoFileNameFromVideoId($videoArchivePath, $videoId);

				rename("./videos/".$videoFileName, "./videos_duplicate/".$videoFileName);
			}
		}

		$command="ffmpeg -i ./data/intro.mp4";

		for($j=0; $j<$numVideos; $j++)
		{
			$command.=" -i ./tmp/".$videos[$j]."_processed.mp4";
		}

		$command.=" -i ./data/outro.mp4";

		$numVideosWithIntroAndOutro=$numVideos+2;

		$command.=" -filter_complex \"";

		for($j=0; $j<$numVideosWithIntroAndOutro; $j++)
		{
			$command.="[".$j.":v][".$j.":a]";
		}

		$compilationNameId=getNumCompilations($compilationsFileName)+1;

		$compilationFileName="The Best of TikTok ".$compilationNameId.".mp4";
		$compilationFileNameEscaped="The\ Best\ of\ TikTok\ ".$compilationNameId.".mp4";

		$command.="concat=n=".$numVideosWithIntroAndOutro.":v=1:a=1[compilationVideo][compilationAudio]\" -map \"[compilationVideo]\" -map \"[compilationAudio]\" ./tmp/".$compilationFileNameEscaped;

		system($command);

		for($j=0; $j<$numVideos; $j++)
		{
			unlink("./tmp/".$videos[$j]."_processed.mp4");
		}

		rename("./tmp/".$compilationFileName, "./compilations/".$compilationFileName);

		unset($compilationLine);

		for($j=0; $j<$numVideos; $j++)
		{
			$compilationLine.=$videos[$j];

			if($j<$numVideos-1)
				$compilationLine.=" ";
			else
				$compilationLine.="\n";
		}

		file_put_contents($compilationsFileName, $compilationLine, FILE_APPEND);
	}
?>