<?php
	function checkVideoId($videoIds, $videoId)
	{
		$videoIdFound=false;

		$numVideoIds=count($videoIds);

		for($i=0; $i<$numVideoIds; $i++)
		{
			if(strcmp($videoIds[$i], $videoId)==0)
			{
				$videoIdFound=true;

				break;
			}
		}

		return $videoIdFound;
	}

	function getVideoIdFromVideoURL($videoURL)
	{
		$videoURLNumBytes=strlen($videoURL);

		$offset=strrpos($videoURL, "/")+1;

		$l=$videoURLNumBytes-$offset;

		$videoId=substr($videoURL, $offset, $l);

		return $videoId;
	}

	function getVideoIdFromVideoFileName($videoFileName)
	{
		$videoFileNameNumBytes=strlen($videoFileName);

		$offset=strpos($videoFileName, "TikTok-")+7;

		$l=($videoFileNameNumBytes-$offset)-4;

		$videoId=substr($videoFileName, $offset, $l);

		return $videoId;
	}

	function getVideoIdsFromVideoArchive($videoArchivePath)
	{
		$videoIds=array();

		$videoFileNames=array();

		$videoFileNames=scandir($videoArchivePath);

		$numVideoFileNames=count($videoFileNames);

		for($i=0; $i<$numVideoFileNames; $i++)
		{
			if(strcmp($videoFileNames[$i], ".")!=0 && strcmp($videoFileNames[$i], "..")!=0 && strcmp($videoFileNames[$i], ".DS_Store")!=0)
				$videoIds[$numVideoIds++]=getVideoIdFromVideoFileName($videoFileNames[$i]);
		}

		return $videoIds;
	}

	$videoArchivePath="videos";

	$videoIds=getVideoIdsFromVideoArchive($videoArchivePath);

	print("TikTok video downloader 1.0 by Julian Meinold\n\n");

	$videoURLsFileName=$argv[1];

	$buf=file_get_contents($videoURLsFileName);

	$videoURLs=explode("\n", $buf);

	$numVideoURLs=count($videoURLs);

	$numVideoURLsProcessed=0;

	for($i=0; $i<$numVideoURLs; $i++)
	{
		if(strlen($videoURLs[$i])>0)
		{
			$videoId=getVideoIdFromVideoURL($videoURLs[$i]);

			if(!checkVideoId($videoIds, $videoId))
			{
				$command="youtube-dl ".$videoURLs[$i]."\n";

				system($command);

				$videoFileName=system("ls *.unknown_video");

				$videoFileNameWithMP4Extension=str_replace(".unknown_video", ".mp4", $videoFileName);

				rename($videoFileName, "./videos/".$videoFileNameWithMP4Extension);

				$numVideoURLsProcessed++;
			}
		}
	}

	print($numVideoURLsProcessed." videos downloaded.\n");
?>