<?php
	function checkVideoId($videoIds, $videoId)
	{
		$videoIdFound=false;

		$numVideoIds=count($videoIds);

		for($i=0; $i<$numVideoIds; $i++)
		{
			if(strcmp($videoIds[$i], $videoId)==0)
			{
				$videoIdFound=true;

				break;
			}
		}

		return $videoIdFound;
	}

	$videoURLsFileName="videourls.txt";

	$userName=strtolower($argv[1]);

	$userProfileURL="https://www.tiktok.com/".$userName;

	$userProfileVideosURL=$userProfileURL."/video/";

	print("TikTok video URL extractor 1.0 by Julian Meinold\n\n");

	print("Attempting to extract video URLs from profile ".$userName."...\n");

	$curlHandle=curl_init();

	curl_setopt($curlHandle, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Safari/605.1.15");
	curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlHandle, CURLOPT_URL, $userProfileURL);

	$buf=curl_exec($curlHandle);

	$bufNumBytes=strlen($buf);

	$userProfileVideosURLNumBytes=strlen($userProfileVideosURL);

	$numVideoURLs=0;

	$videoIds=array();

	$numVideoIds=0;

	for($i=0; $i<$bufNumBytes; $i++)
	{
		$s=substr($buf, $i, $userProfileVideosURLNumBytes);

		if(strcmp($s, $userProfileVideosURL)==0)
		{
			$s1=substr($buf, $i+$userProfileVideosURLNumBytes, 512);

			$videoIdNumBytes=stripos($s1, "\"");

			$videoId=substr($buf, $i+$userProfileVideosURLNumBytes, $videoIdNumBytes);

			if(!checkVideoId($videoIds, $videoId))
			{
				$userProfileVideoURL=$userProfileVideosURL.$videoId;

				$userProfileVideoURLLine=$userProfileVideoURL."\n";

				file_put_contents($videoURLsFileName, $userProfileVideoURLLine, FILE_APPEND);

				$numVideoURLs++;

				$videoIds[$numVideoIds]=$videoId;

				$numVideoIds++;
			}
		}
	}

	print($numVideoURLs." video URLs extracted.\n");
?>